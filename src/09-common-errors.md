# Procédures de corrections d'erreurs fréquentes

## MongoDB

### La Base de données ne se lance plus

Après un arrêt secteur inopiné du VPS, il peut y avoir une désynchronisation qui empêche WiredTiger de récupérer correctement les données et mongodb de se lancer.

Attention : N'hésitez pas à copier la base de données via rsync ou dd afin d'avoir une copie des fichiers corrompus en cas d'échec de la réparation.

Pour résoudre ça :
```bash
systemctl stop mongodb
mongod -f /etc/mongod.conf --repair
systemctl start mongodb
```

## NodeJS

### Erreur de "undefined symbol" d'une dépendance C/C++ (tel que bcrypt)

Dans le cas où l'architecture est différente, il peut être nécessaire de reconstruire une dépendance depuis ses sources embarquées dans le livrable.
Pour cela, il sera nécessaire d'avoir `gcc`, `g++` et `make` au préalable. On pourrait recommander l'installation du meta-packet 'Development Tools' (`yum groupinstall 'Development Tools'`).

Lancez ensuite `npm rebuild <package> --build-from-source`, par exemple `npm rebuild bcrypt --build-from-source`.
